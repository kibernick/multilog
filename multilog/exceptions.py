class BreakLoop(Exception):
    """Used by tests to tell the scheduler to stop fetching results."""
