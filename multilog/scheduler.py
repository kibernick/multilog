import json
import multiprocessing as mp
from queue import Empty as QueueEmpty

from .config import NUM_TASKS, NUM_WORKERS, QUEUE_TIMEOUT, OUTPUT_FILE
from .exceptions import BreakLoop
from .logs import setup_scheduler_logging
from .queue import log_queue, results_queue
from .worker import worker


def scheduler(num_workers: int = NUM_WORKERS,
              num_tasks: int = NUM_TASKS,
              results_queue: mp.Queue = results_queue,
              log_queue: mp.Queue = log_queue):
    """Parent process which kicks off the workers.

    :param num_workers: Number of workers in a worker process pool.
    :param num_tasks: Number of tasks to distribute.
    :param results_queue: Queue to retrieve worker results from.
    :param log_queue: Queue used for logging system messages.
    """
    logger, queue_listener = setup_scheduler_logging(log_queue)
    queue_listener.start()
    logger.warning("Logger started!")

    pool = mp.Pool(processes=num_workers)
    logger.info(f"Starting {num_workers} workers, with {num_tasks} tasks.")
    [pool.apply_async(worker, args=(f'Bob{i}',)) for i in range(num_tasks)]

    with open(OUTPUT_FILE, mode='a') as output_file:
        while True:
            continue_loop = _scheduler_main_loop(results_queue, output_file, logger)
            if not continue_loop:
                break

    queue_listener.stop()
    output_file.close()


def _scheduler_main_loop(queue, output_file, logger) -> bool:
    """Inner loop of the scheduler - returns True for loop to continue,
    False in case of other exceptions (except QueueEmpty) caught."""
    try:
        result = queue.get(block=True, timeout=QUEUE_TIMEOUT)
        logger.info(f"Result received: {result}")
        json.dump(result, output_file)
        output_file.write('\n')  # Line-delimited JSON.
        return True

    except QueueEmpty:
        logger.warning("Queue appears to be empty.")
        return True

    except TypeError as e:
        logger.error(f"Error writing to file: {e}")
        return False
    except BreakLoop:
        return False
    except Exception as e:
        logger.error(f"Exception: {e}")
        return False
