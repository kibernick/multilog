import multiprocessing as mp

"""Queue for logging process messages."""
log_queue = mp.Queue()

"""Workers will push their results to this queue."""
results_queue = mp.Queue()
