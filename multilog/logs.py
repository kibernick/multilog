import logging
import multiprocessing as mp
from logging.handlers import QueueHandler, QueueListener

from .config import LOG_FORMAT, LOG_FILE


def setup_scheduler_logging(log_queue: mp.Queue) -> (logging.Logger, QueueListener):
    """Setup the logger of the scheduler process, and a listener for logs from the workers.

    The scheduler aims to consolidate both its and the workers logs in the same output file.

    :param log_queue: Queue used for logging system messages.
    """
    logger = mp.get_logger()
    logger.setLevel(logging.INFO)

    file_handler = logging.FileHandler(LOG_FILE)
    file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    logger.addHandler(console_handler)

    queue_listener = QueueListener(log_queue, file_handler)

    return logger, queue_listener


def setup_worker_logger(log_queue: mp.Queue) -> logging.Logger:
    """Setup the logger of the worker process.

    The worker process will log its messages to a queue and let the parent process handle them.

    :param log_queue: Queue used for logging system messages.
    """
    queue_handler = QueueHandler(log_queue)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.addHandler(queue_handler)

    return logger
