"""Multiprocessing settings."""
NUM_WORKERS = 4
NUM_TASKS = 10
QUEUE_TIMEOUT = 5  # Number of seconds to block for results

"""Logging settings."""
LOG_FORMAT = '%(asctime)s - %(process)d - %(levelname)s - %(message)s'
LOG_FILE = 'process.log'

"""Output settings."""
OUTPUT_FILE = 'output.json'
