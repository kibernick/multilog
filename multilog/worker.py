import multiprocessing as mp
import random
from time import sleep
from hashlib import md5

from .logs import setup_worker_logger
from .queue import log_queue, results_queue


def worker(input_string: str,
           simulate_hard_work: bool = True,
           results_queue: mp.Queue = results_queue,
           log_queue: mp.Queue = log_queue):
    """A simple worker process that generates an MD5 hash of a string.

    :param input_string: Will be encoded into an MD5 hash.
    :param simulate_hard_work: Simulate a long-running process!
    :param results_queue: Queue to push results to.
    :param log_queue: Queue used for logging system messages.
    """
    logger = setup_worker_logger(log_queue)

    logger.info(f"Received some work: {input_string}")

    if simulate_hard_work:
        sleep(random.random() * 2)  # Wait an interval of [0, 2) seconds.

    try:
        hasher = md5()
        hasher.update(input_string.encode('ascii'))
        md5_hash = hasher.hexdigest()

        results_queue.put({
            'input': input_string,
            'output': md5_hash,
            'pid': mp.current_process().pid,
        })
        logger.info("Job's done!")
    except Exception as e:  # TODO: Not the best way to handle exceptions :)
        logger.error(f"{type(e).__name__}: {e}")
