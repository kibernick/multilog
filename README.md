# Multilog

![Logging...](images/logging.jpg "logging")

## Task 

Implement the parent logging mechanism, and the communication between parent and child processes, to make sure the logging of all spawned processes is centralized by the parent. Demonstrate the functionality in a self-contained example.

## Solution

The solutions makes use of Python's `multiprocessing` library and involves the following components:

* A [Scheduler](multilog/scheduler.py) (the main process) which contains a `Pool` of workers  to distribute tasks to, as well as a centralized logging location.
* [Worker](multilog/worker.py) processes which take an input string, calculate its MD5 hash and outputs the result to a queue. The logs that the workers generate are also pushed a (logging) queue.
* [Queues](multilog/queue.py) are used for inter-process communication. Two queues are used - one for passing messages between worker and scheduler, another for centralized logging.

### Approach

* The parent process controls a `Pool` of worker processes, to which it distributes tasks. We use the `apply_async` method to kick them off without blocking.

* We use message-passing for inter-process communication, to avoid sharing memory, since that would open up a lot of concurrency issues common in parallel programming.
    * If sharing of memory was required, we would use other concurrency primitives available in Python, such as `Lock`, `Value` and `Manager`.

* The results of the calculations done by the workers are retrieved from a `Queue`. They are aggregated in a single `output.json` file.

* To consolidate logs across multiple processes, we use the `QueueHandler` and `QueueListener` handlers provided by Python's `logging` module. We utilize a `Queue` to pass logging messages from the workers and then listen for them in the parent process. 
    * We can identify the source of the log message by the process id.

### Running the commands

The following Make commands are available:

`make run` - to start the distributed processes

`make test` - to run the tests

### Outputs

* `output.json` is a line-delimited JSON containing the outputs of the worker processes.
* `process.log` represents the consolidated system logs from the scheduler and the workers in one place.

## Improvements

* Use command-line arguments to override the [default values](multilog/config.py) such as the output file to log to, and number of workers.
* Workers can be made to pick up new tasks from the queue, in a trully distributed fashion, instead of being directly assigned tasks by the scheduler.
