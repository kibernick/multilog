from unittest import mock

import pytest


@pytest.fixture
def log_queue():
    return mock.MagicMock()


@pytest.fixture
def results_queue():
    return mock.MagicMock()
