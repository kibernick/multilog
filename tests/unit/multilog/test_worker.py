from logging import Logger
from unittest import mock

import pytest

from multilog.worker import worker


@pytest.mark.parametrize('test_input,expected', [
    ('123', '202cb962ac59075b964b07152d234b70'),
    ('Team Rocket', 'c414508380deb5d47fd1e0bd8682a7c5'),
])
@mock.patch('multilog.worker.setup_worker_logger')
def test_worker(setup_logging, test_input, expected, log_queue, results_queue):
    logger = mock.MagicMock(Logger)
    setup_logging.return_value = logger

    worker(test_input,
           simulate_hard_work=False,
           results_queue=results_queue,
           log_queue=log_queue)

    result = results_queue.method_calls[0][1][0]
    assert result['input'] == test_input
    assert result['output'] == expected


@pytest.mark.parametrize('test_input', [None, 321])
@mock.patch('multilog.worker.setup_worker_logger')
@mock.patch('multilog.worker.sleep')
def test_worker_input_errors(mock_sleep, setup_logging, test_input,
                             log_queue, results_queue):
    logger = mock.MagicMock(Logger)
    setup_logging.return_value = logger
    mock_sleep.return_value = None

    worker(test_input,
           simulate_hard_work=False,
           results_queue=results_queue,
           log_queue=log_queue)

    results_queue.assert_not_called()
    assert logger.method_calls[1][1][0].startswith('AttributeError')
