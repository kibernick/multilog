from queue import Empty as QueueEmpty
from unittest import mock

import pytest

from multilog.scheduler import (
    scheduler,
    _scheduler_main_loop,
    BreakLoop,
)


@pytest.mark.parametrize('num_workers', [1, 2])
@pytest.mark.parametrize('num_tasks', [1, 2])
@mock.patch('multilog.scheduler.setup_scheduler_logging')
@mock.patch('multilog.scheduler.mp.Pool')
def test_scheduler(pool, setup_logging, log_queue, results_queue,
                   num_tasks, num_workers):
    logger, listener = mock.MagicMock(), mock.MagicMock()
    setup_logging.return_value = logger, listener
    results_queue.get.side_effect = [f'result {n}' for n in range(num_tasks)] + [BreakLoop('done')]
    mock_open = mock.mock_open()

    with mock.patch('multilog.scheduler.open', mock_open):
        scheduler(
            num_workers=num_tasks,
            num_tasks=num_workers,
            results_queue=results_queue,
            log_queue=log_queue,
        )

    setup_logging.assert_called_with(log_queue)
    assert pool.called, "Worker pool was never setup"
    assert results_queue.get.called, "Results were never fetched"
    assert logger.method_calls, "Logger was never called"
    for n in range(num_tasks):
        result_str = f'"result {n}"'
        assert mock.call().write(result_str) in mock_open.mock_calls, f"{result_str} not written to output file"


def test_inner_loop(results_queue):
    output_file, logger = mock.MagicMock(), mock.MagicMock()
    results_queue.get.return_value = ':partyparrot:'

    continue_loop = _scheduler_main_loop(results_queue, output_file, logger)

    assert continue_loop
    assert output_file.method_calls == [
        mock.call.write('":partyparrot:"'),
        mock.call.write('\n'),
    ]


def test_inner_loop_empty_queue(results_queue):
    output_file, logger = mock.MagicMock(), mock.MagicMock()
    results_queue.get.side_effect = QueueEmpty

    continue_loop = _scheduler_main_loop(results_queue, output_file, logger)

    assert continue_loop
    output_file.assert_not_called()


@pytest.mark.parametrize('exception', [TypeError, BreakLoop, ZeroDivisionError])
def test_inner_loop_exceptions(results_queue, exception):
    output_file, logger = mock.MagicMock(), mock.MagicMock()
    results_queue.get.side_effect = exception

    continue_loop = _scheduler_main_loop(results_queue, output_file, logger)

    assert not continue_loop
    output_file.assert_not_called()
